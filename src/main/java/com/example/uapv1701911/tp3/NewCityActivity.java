package com.example.uapv1701911.tp3;

import android.app.AlertDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewCityActivity extends AppCompatActivity {

    private TextView textName, textCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        final WeatherDbHelper dbHelper = new WeatherDbHelper(NewCityActivity.this);

        textName = (EditText) findViewById(R.id.editNewName);
        textCountry = (EditText) findViewById(R.id.editNewCountry);

        final Button but = (Button) findViewById(R.id.button);
        but.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                City city = new City(textName.getText().toString(),textCountry.getText().toString());
                dbHelper.addCity(city);
                finish();
            }

        });
    }
}
